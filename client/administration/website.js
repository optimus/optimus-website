export default class AdministrationWebsite
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	async init()
	{
		if (admin_only()) return

		await load('/services/optimus-website/administration/website.html', this.target)

		this.target.querySelector('.domain').innerText = store.user.server.replace('api.', 'www.')
		this.target.querySelector('.domain').onclick = () => window.open('https://' + this.target.querySelector('.domain').innerText)

		let website = await rest('https://' + store.user.server + '/optimus-base/server/preferences/website', 'GET', {}, null).then(result => result.code == 200 && result.data) || '404'

		let redirect_type = website.split(' ')[0]
		this.target.querySelectorAll('input[name="redirect_type"]').forEach(radio => 
		{
			if (radio.value == redirect_type)
				radio.checked = true
		})

		if (website.includes(' http'))
		{

			this.target.querySelector('.redirect_http_scheme').value = 'http://'
			this.target.querySelector('.proxypass_http_scheme').value = 'http://'
			this.target.querySelector('.redirect_website').value = website.split(' ')[1].split('//')[1]
			this.target.querySelector('.proxypass_website').value = website.split(' ')[1].split('//')[1]
		}

		this.target.querySelector('.apply-button').onclick = event =>
		{
			let object = null
			if (this.target.querySelector('input[name="redirect_type"]:checked').value == 301)
				object = this.target.querySelector('.redirect_website')
			if (this.target.querySelector('input[name="redirect_type"]:checked').value == 'proxypass')
				object = this.target.querySelector('.proxypass_website')

			if (object)
			{
				if (!object.value.match(/^(\*)$|^(localhost|(\*\.)?([a-z0-9][a-z0-9-]*[a-z0-9]\.)+[a-z0-9][a-z0-9-]*[a-z0-9]|(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:\.(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3})(:((6553[0-5])|(655[0-2][0-9])|(65[0-4][0-9]{2})|(6[0-4][0-9]{3})|([1-5][0-9]{4})|([0-5]{0,5})|([0-9]{1,4})))?$/))
				{
					object.classList.add('is-danger')
					object.setCustomValidity('Merci de renseigner une adresse IP ou un nom de domaine. Exemples : www.optimus.fr, 134.202.25.4, 115.28.4.20:9000')
					object.reportValidity()
					object.onkeyup = () => 
					{
						object.setCustomValidity('')
						object.reportValidity()
						object.classList.remove('is-danger')
					}
					return false
				}
			}

			let value = this.target.querySelector('input[name="redirect_type"]:checked').value
			if (this.target.querySelector('input[name="redirect_type"]:checked').value == '301')
				value += ' ' + this.target.querySelector('.redirect_http_scheme').value + this.target.querySelector('.redirect_website').value
			else if (this.target.querySelector('input[name="redirect_type"]:checked').value == 'proxypass')
				value += ' ' + this.target.querySelector('.proxypass_http_scheme').value + this.target.querySelector('.proxypass_website').value

			rest('https://' + store.user.server + '/optimus-base/server/preferences/website', 'POST', value)
				.then(result => 
				{
					if (result.code == 201)
					{
						optimusToast('Redémarrage du serveur web en cours...', 'is-warning')
						rest('https://' + store.user.server + '/optimus-base/services/optimus-website/start', 'POST')
							.then(response => response.code == 200 && optimusToast('Le serveur web a redémarré avec succès', 'is-success'))
					}
				})
		}
	}
}