export default class OptimusWebsiteService
{
	constructor()
	{
		this.name = 'optimus-website'
		if (!admin_only())
			this.optimus_base_administration_tabs =
				[
					{
						id: "tab_website",
						text: "Site web",
						link: "/services/optimus-website/administration/website.js",
						position: 700
					}
				]
		store.services.push(this)
	}

	login()
	{

	}

	logout()
	{
		store.services = store.services.filter(service => service.name != this.id)
	}

	dashboard()
	{

	}
}